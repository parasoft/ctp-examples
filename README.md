# Parasoft Continous Testing Platform intergrations with GitLab

This project contains an example .gitlab-ci.yml file to demonstrate how to intergrate Parasoft CTP with your workflow.

There are four example jobs in /actions that describes a complete workflow. 

### 1. Deploy environment

This job enables you to stand up a consistent, disposable environment for testing your application. It deploys and provisions a Parasoft service virtualization environment to the specified Continous Testing Platform endpoint.

### 2. Execute job

This job executes a test scenario job located at the specified Parasoft Continuous Testing Platform endpoint.

### 3. Destroy environment

This job allows you to destroy a Parasoft service virtualization environment in a given Continous Testing Platform endpoint.

### 4. Disconnect server

This job allows you to disconnect a server in a given Continous Testing Platform endpoint.

## Prerequisites

1. Running CTP server reachable by GitLab

## Usage

Please see [Example YAML](/.gitlab-ci.yml) for a complete example.
Add the following entry to your GitLab CI/CD YAML file to include these example jobs:
```
include: 
  - project: 'parasoft/ctp-examples'
    ref: main
    file:
      - '/actions/deploy-environment.yml'
      - '/actions/destroy-environment.yml'
      - '/actions/disconnect-server.yml'
      - '/actions/execute-job.yml'

```
Once the jobs are included, please define the global variables that are shared accross the examples.
```
# Common variables shared with all CTP actions
variables:
  CTP_URL: "http://ctp.server:8080/em" 
  CTP_USERNAME: "username"
  CTP_PASSWORD: "password"
```
| Variable | Description |
| --- | --- |
| `CTP_URL` | Specifies the Continuous Testing Platform endpoint where the environment will be deployed. |
| `CTP_USERNAME` | Specifies a username for accessing the Continuous Testing Platform endpoint. |
| `CTP_PASSWORD` | Specifies a password for accessing the Continuous Testing Platform endpoint. |

## Example jobs

It is recommended to use the `needs : [job-name]` tag in the jobs to maintain the workflow order:
Deploy environment > Excute job > Destroy environment > Disconnect server

### Deploy environment

Add the following entry to your GitLab CI/CD YAML file and provide job specific variables to define the deploy environment example:

```
deploy-environment: 
  stage: test
  variables:
    CTP_SYSTEM: "system name"
    CTP_ENVIRONMENT: "environment name" 
    CTP_INSTANCE: "instance name"
```
##### Required Variables

| Variable | Description |
| --- | --- |
| `CTP_SYSTEM` | Specifies the name of the system in Continous Testing Platform with the environment instance to provision. |
| `CTP_ENVIRONMENT` | Specifies the name of the environment with instances to provision. |
| `CTP_INSTANCE` | Specifies the environment instance to provision. |

##### Optional Variables

| Variable | Description |
| --- | --- |
| `COPY_ENVIRONMENT` | Copy the environment and assets before provisioning. |
| `NEW_ENVIRONMENT_NAME` | New enviornment name when COPY_ENVIRONMENT is true. |
| `VIRTUALIZE_SERVER_NAME` | Copy to a Virtualize server matching name when COPY_ENVIRONMENT is true. |

### Execute job

Add the following entry to your GitLab CI/CD YAML file and provide job specific variables to define the execute job example:

```
execute-job:  
  stage: test
  needs: [deploy-environment]
  variables:
    CTP_JOB: "job name"
```
##### Required Variables

| Variable | Description |
| --- | --- |
| `CTP_JOB` | Specifies the name of the job in Continous Testing Platform to execute. |


### Destroy environment

Add the following entry to your GitLab CI/CD YAML file and provide job specific variables to define the destroy environment example:

```
destroy-environment:
  stage: test
  needs: [execute-job]
  variables:
    CTP_SYSTEM: "system name"
    CTP_ENVIRONMENT: "environment name"
```

##### Required Variables

| Variable | Description |
| --- | --- |
| `CTP_SYSTEM` | Specifies the name of the system in Continous Testing Platform with the environment to delete. |
| `CTP_ENVIRONMENT` | Specifies the name of the environment to delete. |



### Disconnect server

Add the following entry to your GitLab CI/CD YAML file and provide job specific variables to define the disconnect example:

```
disconnect-server:
  stage: test
  needs: [destroy-environment]
  variables:
    SERVER_MATCH: "name"
    SERVER: "server name"
```
Note: use `when: manual` tag during configuration to only disconnect server when necessary

##### Required Variables

| Variable | Description |
| --- | --- |
| `SERVER_MATCH` | Specifies whether to find server by "name" or "host". |
| `SERVER` | Specifies the name or host of a server to disconnect. |
